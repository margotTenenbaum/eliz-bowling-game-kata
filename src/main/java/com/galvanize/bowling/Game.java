package com.galvanize.bowling;

import java.util.ArrayList;

public class Game {
    private int score = 0;
    private ArrayList<Integer> rolls = new ArrayList<>();


    public void roll(int pins) {
        rolls.add(pins);
    }

    public int score() {
        for (int i = 0; i < rolls.size(); i+=2) {
            int frame = 0;
            //if strike
            if (rolls.get(i) == 10) {
                score+= 10;
                if (i <= rolls.size() - 3) {
                    score+= rolls.get(i+1);
                    score+= rolls.get(i+2);
                    if (i == rolls.size() - 3) {
                        break;
                    }
                }
                i--;
            } else {
                if (i <= rolls.size() - 2) {
                    frame+= rolls.get(i);
                    frame+= rolls.get(i+1);
                    //if spare
                    if (frame == 10) {
                        score+= 10;
                        score+= rolls.get(i+2);
                    } else {
                        score+= frame;
                    }
                }
            }
        }
        return score;
    }

    public int getScore() {
        return score;
    }
}


