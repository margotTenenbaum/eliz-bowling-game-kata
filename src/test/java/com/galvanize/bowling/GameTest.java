package com.galvanize.bowling;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GameTest {
    Game testGame;

    @BeforeEach
    void setup() {
        testGame = new Game();
    }
    @Test
    void canCreateNewGameWithScoreZero() {
        int expected = 0;
        int actual = testGame.getScore();
        assertEquals(expected, actual);
    }

    @Test
    void canScore20GutterBallGame() {
        int expected = 0;
        for (int i = 0; i < 20; i++) {
            testGame.roll(0);
        }
        int actual = testGame.score();
        assertEquals(expected, actual);
    }

    @Test
    void canScoreGameOf20SinglePinRolls() {
        int expected = 20;
        for (int i = 0; i < 20; i++) {
            testGame.roll(1);
        }
        int actual = testGame.score();
        assertEquals(expected, actual);
    }

    @Test
    void canScoreGameWithOneSpare() {
        int expected = 19;
        testGame.roll(5);
        testGame.roll(5);
        testGame.roll(3);
        testGame.roll(3);
        for (int i = 0; i < 16; i++) {
            testGame.roll(0);
        }
        int actual = testGame.score();
        assertEquals(expected, actual);
    }

    @Test
    void canScoreGameWithOneStrike() {
        int expected = 22;
        testGame.roll(10);
        testGame.roll(3);
        testGame.roll(3);
        for (int i = 0; i < 16; i++) {
            testGame.roll(0);
        }
        int actual = testGame.score();
        assertEquals(expected, actual);
    }

    @Test
    void canScoreAPerfectGame() {
        int expected = 300;
        for (int i = 0; i < 12; i++) {
            testGame.roll(10);
        }
        int actual = testGame.score();
        assertEquals(expected, actual);
    }

    @Test
    void canScoreAGameWhereLastFrameIsASpare() {
        int expected = 84;
        for (int i = 0; i < 18; i++) {
            testGame.roll(4);
        }
        testGame.roll(5);
        testGame.roll(5);
        testGame.roll(2);
        int actual = testGame.score();
        assertEquals(expected, actual);
    }

    @Test
    void canScoreAGameWhereLastFrameIsAStrike() {
        int expected = 86;
        for (int i = 0; i < 18; i++) {
            testGame.roll(4);
        }
        testGame.roll(10);
        testGame.roll(2);
        testGame.roll(2);
        int actual = testGame.score();
        assertEquals(expected, actual);
    }

    @Test
    void canScoreMixOfSparesAndStrikes() {
        int expected = 181;
        testGame.roll(7);
        testGame.roll(3);
        testGame.roll(7);
        testGame.roll(2);
        testGame.roll(10);
        testGame.roll(10);
        testGame.roll(10);
        testGame.roll(3);
        testGame.roll(1);
        testGame.roll(0);
        testGame.roll(10);
        testGame.roll(5);
        testGame.roll(4);
        testGame.roll(10);
        testGame.roll(10);
        testGame.roll(10);
        testGame.roll(10);
        int actual = testGame.score();
        assertEquals(expected, actual);

    }

}